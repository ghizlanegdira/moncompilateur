			# This code was produced by the CERI Compiler
	.data
	.align 8
FormatString1:	.string "%llu\n"	
a:	.quad 0
b:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
for0:
	push $1
	pop a
TO0:
	push $5
	push a
	pop %rbx
	pop %rax
	cmpq %rax, %rbx
	ja FINFOR0
DO0:
	push $1
	pop b
	addq $1,a
	jmp TO0
FINFOR0:
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
